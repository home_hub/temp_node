#pragma once

/**
 * @file connection_handler.h
 * @author Grzegorz Rezmer <grzes_rezmer AT o2.pl>
 * @version 1.0.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 *  Common functions that should be used to receive, parse and send data. If you want to make modyfications for commands
 *  and functionaliti of state  machine this is the proper place. 
 */

#include <avr/common.h>

 /**
  * \ingroup Functions
  * 
  * Message_parser function is madeto parse incomming messages with define state machine. It checks that
  * message has proper lenght and propper AT command format. If it don't recognize format or messege will be wrong
  * it will send to TX address proper message: "AT+Error"
  * 
  * @param nRF_RX_buff Pointer to buffer with received message.
  * @param len Variable with message lenght. 
  * 
  * @return void.
  */
void message_parser(uint8_t *nRF_RX_buff, uint8_t len);

 /**
  * \ingroup Functions
  * command_parser is parsing ready command and doing dedicated action. For now there are two commands:
  *     * "PING" - then it return "AT+PONG" to TX address
  *     * "DATA" - then it return sensor reading to TX address.
  * 
  * @param nRF_RX_buff Pointer to buffer with command. 
  * 
  * @return void.
  */
void command_parser(uint8_t *buffer);

 /**
  * \ingroup Functions
  * send_readings get data from DHT sensor and create message to be send to TX addres.
  * For secuirity global interupts for reading sesnor time are desabled, to do not disturb reading proces (it's time dependent)
  * It's designed to send readings as special pocket with special data atributes. See descryption in main.  
  * 
  * In the worst case reading sensor values should take 6ms. 
  * 
  * @param nRF_RX_buff Pointer to buffer with received message. 
  * 
  * @return void.
  */
void send_readings();

 /**
  * \ingroup Functions
  * 
  * Function to init connection handler internal struct for data.
  * it schould be called durning cpu init.
  * 
  * @return void.
  */
void init_connection_handler();