/**
 *  
 * * @author Grzegorz Rezmer <grzes_rezmer AT o2.pl>
 * @version 1.0.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 *  Common functions that should be used to receive, parse and send data. If you want to make modyfications for commands
 *  and functionaliti of state  machine this is the proper place. 
 */

#include <avr/common.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <stdlib.h>

#include "DHT22.h"
#include "connection_handler.h"
#include "radio.h"
#include "sensors_protocol.h"

#define STATE_1 1   ///< State machine state variable- Start, check AT format
#define STATE_2 2   ///< State machine state variable - check AT format
#define STATE_3 3   ///< State machine state variable - parse propper command
#define STATE_ERR 4 ///< State machine state variable - Error handler state
#define STATE_END 5 ///< State machine state variable - End state if everything went good.

static uint8_t cmd_buffer[6]; ///< Buffer for commands parsed by message parser.
char dataBuffer[14]; ///< Sending readings buffer.

static DHT22_ERROR_t error;
static MsgHeader_t sensorMsgHeader;

void init_connection_handler()
{
		sensorMsgHeader.id = DEVICE_ID;
		sensorMsgHeader.msg_type.cmd = 0;
		sensorMsgHeader.sensorType = DHTHumAndTempNode; 
}

void command_parser(uint8_t *buffer)
{

	if (strncmp((char *)buffer, "PING", 5) == 0)
	{
		radio_transmit_P(PSTR("AT+PONG"), 8);
	}
	else if (strncmp((char *)buffer, "DATA", 5) == 0)
	{
		send_readings();
	}
	else
	{
		radio_transmit_P(PSTR("AT+E_bad_comand"), 16);
	}
}

void message_parser(uint8_t *nRF_RX_buff, uint8_t len)
{
	DEBUG_LED_LOW;
	uint8_t i = 0;
	uint8_t state = STATE_1;
	uint8_t *wsk;

	do
	{
		switch (state)
		{
		case STATE_1:
			if ((nRF_RX_buff[0] == 'A') && (nRF_RX_buff[1] == 'T'))
			{
				state = STATE_2;
				break;
			}
			else
			{
				state = STATE_ERR;
				break;
			}

		case STATE_2:
			if (nRF_RX_buff[2] == '+')
			{
				state = STATE_3;
				break;
			}
			else
			{
				state = STATE_ERR;
				break;
			}

		case STATE_3:
			wsk = &nRF_RX_buff[3];
			while (1)
			{
				if ((*wsk == '\r') || (*wsk == '\0'))
					break;

				cmd_buffer[i] = *wsk++;
				i++;
			}
			command_parser(cmd_buffer);

			state = STATE_END;
			break;

		case STATE_ERR:
			radio_transmit_P(PSTR("AT+Error"), 9);
			state = STATE_END;
			break;
		}

	} while (state != STATE_END);
	state = STATE_1;
}

void send_readings()
{
	TempHumData_t sensor_values = { 0 };
	SensorMsg_t sensorMsg = {
		.header = sensorMsgHeader,
	};

	cli();
	error = readDHT22((DHT22_DATA_t *)&sensor_values);
	sei();

	sensorMsg.data_u.DHTtempHumData = sensor_values;
	radio_transmit(&sensorMsg,sizeof(sensorMsg));
}