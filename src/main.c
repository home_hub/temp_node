/**
 * @file 
 * @author Grzegorz Rezmer <grzes_rezmer AT o2.pl>
 * @version 1.0.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 * 
 * This is entrypoint of all program. Some important informations ale below. 
 * 
 *	<b>IMPORTANT</b>
 * 
 *<b>Used interupts:</b>
 *    
 *  *  SPI: TIMER0_COMPA_vect, USI_OVERFLOW_vect
 *  *  nRF: INT0 - interupt vector falling edge
 * 	*  sleap: WDT_OVERFLOW_vect - to interrupt durning sleep mode. 
 *     
 *<b>Radio Configuration:</b> 
 *	*	Transmit channel: 120 
 *	*	Transmit speed: 1MB
 *	*	RX/TX addres: chose in common.h
 *
 * <b>Transport Protocol</b>
 * 
 * <b>!!! DEPRECEIATED!!!</b>
 * To proper send data the special transport protocol was develop:
 * 
 *	<HEADER><DEVICETYPE>,<DATA><END>
 * 
 * Where: 
 * 	Header: is always: AT+ and has 3 bytes
 * 	Device: type is devie type number: 1,2,3 etc. is allways uitn8_t
 * 	Data: is data type what kaind is needed by the devie. 
 * 	End: is asci \0 'NULL' sign. 
 * 	
 * 	All pocekt data should be maximum 32 bytes long. So exluding header, devicetype and end
 * 	data section should have maximum 27 bytes. 
 * 		 	
 *****/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/sleep.h>

#include "radio.h"
#include "DHT22.h"
#include "connection_handler.h"

#define VERSION "v.1.0.1"

volatile static uint8_t get_reading; ///< State variable called to send reading;
volatile static uint8_t counter;
volatile static uint8_t sleep;

void initWDT()
{
	wdt_reset();
	cli();
	MCUSR &= ~_BV(WDRF);          // clear reset flag		
	WDTCSR |= _BV(WDCE) | _BV(WDE);
	WDTCSR = 0;
	//WDTCSR |= _BV(WDIE) | _BV(WDP3); // 4 s 
	WDTCSR |= _BV(WDIE) | _BV(WDP3) | _BV(WDP0); // 8 s
	//WDTCSR |= _BV(WDIE) | _BV(WDP2) | _BV(WDP1); // 1 s
	sei();
}

void goToSleep()
{	
	cli();
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable(); // Enable Sleep Mode
	ACSR |= _BV(ACD); // disable the ADC
	wdt_reset();	
	sei();			  // Enable Interrupts
	sleep_cpu();	  // Go to Sleep
}

int main(void)
{
	get_reading = 1;

	init_connection_handler();
	radio_register_RX_Callback(message_parser);
	radio_init();
	initWDT();
	sei();
	
	while (1)
	{
		if(sleep)
		{			
		 	radio_Power_Down();
			goToSleep();
		}

		radio_RX_EVENT();
		
		if (get_reading)
		{   
			get_reading = 0;
			sleep = 1;
			wdt_reset();
			radio_init();
			_delay_ms(5);
			send_readings();
			_delay_ms(100);
		}
	}
}

ISR(WDT_OVERFLOW_vect)
{	
	sleep_disable();	

	WDTCSR |= _BV(WDIE);

	if (counter != (SLEEP_TIME -1))
	{	
		counter+=1;
	}
	else
	{
		get_reading = 1;
		sleep = 0;
		counter = 0;
		sleep_enable();
	}
}
