#ifndef COMMON_H
#define COMMON_H

/**
 * @file 
 * @author Grzegorz Rezmer <grzes_rezmer AT o2.pl>
 * @version 1.0.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 * 
 * File used for group all common wariables and definitions. It should be used for configurating your project. 
 * 
 * DO NOT CHENGE NAMES OF DEFINES AND DO NOT CHENGE ANYTHING IN OTHER FILES REGARDING TO THIS VARIABLES!!! 
 * 		 	
 *****/

/***** NODE ADDRESS CONFIGURATION ***/

/**
 * \defgroup Functions
 * \brief List of defined adresses for communication with node.
 * Define one of theise adresses to comunicate with Hub.
 * For correct connection each node should have indyvidual addres.
 */

// #define ADDRES "1NODE"  ///< Address for pipe: max 5 bytes;
 #define ADDRES "2NODE"  ///< Address for pipe: max 5 bytes;
// #define ADDRES "3NODE"  ///< Address for pipe: max 5 bytes;
// #define ADDRES "4NODE"  ///< Address for pipe: max 5 bytes;
// #define ADDRES "5NODE"  ///< Address for pipe: max 5 bytes;

/**
 * \defgroup Functions
 * \brief To setup adress lengh of the node. Default is 5 bytes. 
 */

#define ADDRES_LENGTH 5 ///< Address lenght for pipe: max 5 bytes;

/********  CONFIGURATION SLEEP TIME *********/ 
/**
 * \defgroup Functions
 * \brief In here you may want to configure time beetwen mesurmants. 
 */
#define SLEEP_TIME 1  ///< Number of intervals (8 seconds) between mesurments 

/**
 * \defgroup Functions
 * \brief Device ID to hub configuration. ID should be unique!.
 */
#define DEVICE_ID 12345; ///< DEFINE WHAT IS DEVICE ID. 

// /**
//  * \defgroup Functions
//  * \brief Configure witch is this device type. type 1 is for temp and hum device.
//  */
// #define DEVICETYPE 1 ///< DEFINE WITCH IS THIS DEVICE TYPE

// /**
//  * \defgroup Functions
//  * \brief Define wath is device name to show in hub.
//  */
// #define DEVICENAME "SALON" ///< MAX 10 Bytes.

/**
 * \defgroup Functions
 * \brief List of all important functions in project
 */
#define DDR_SPI DDRB ///< SPI port register 
#define DD_DO PB6   ///< SPI Data Out Pin
#define DD_DI PB5   ///< SPI Data In Pin
#define DD_SCK PB7  ///< SPI Clock Pin
#define DD_SS CSN_PIN ///< SPI Chip select negative (low) Pin

#define DDR_CE_CSN DDRB ///< nRF CE nad CSN Pins data register
#define CSN_PORT PORTB ///< nRF CSN Port
#define CE_PORT PORTB ///< nRF CE Port

#define CE_PIN PB1 ///< nRF CE Pin
#define CSN_PIN PB0 ///< nRF CSN pin
#define IRQ_PIN PD2 ///< nRF IRQ pin

#define CE_HI CE_PORT |= (1 << CE_PIN) ///< nRF CE pin direction out
#define CE_LOW CE_PORT &= ~(1 << CE_PIN) ///< nRF CE pin direction in
#define CSN_HI CSN_PORT |= (1 << CSN_PIN) ///< nRF CSN pin direction out
#define CSN_LOW CSN_PORT &= ~(1 << CSN_PIN) ///< nRF CSN pin direction in

#define NUMBER_OF_DATA_PIPES 1 ///< number of data pipes: 1 -5 data RX PIPES
#define TRANSMITT_CHANNEL 75   ///< number of transmit channel: 1 - 125 number of channels
#define ADDRES_BYTE_LENGHT 5   ///< size of addres in Bytes: 1 - 5 

#define MAXIMUM_PAYLOAD_SIZE 15 ///< Maximum payload size to receive
#define BUFFER_SIZE 15 ///< define maximum RX buffer size

/** Debug Led for Debugind pouposes ****/

#define DEBUG_LED PD6 ///< DEBUG Led Pin 
#define DEBUG_LED_PORT PORTD ///< DEBUG Led port 
#define DEBUG_LED_DIR_REGISTER DDRD ///< DEBUG directory register

#define DEBUG_LED_OUT DEBUG_LED_DIR_REGISTER |= (1<<DEBUG_LED) ///< DEBUG Led pin direction out
#define DEBUG_LED_IN DEBUG_LED_DIR_REGISTER &= ~(1<<DEBUG_LED) ///< DEBUG Led pin low state.

#define DEBUG_LED_HIGH DEBUG_LED_PORT |= (1<<DEBUG_LED) ///< DEBUG Led ON
#define DEBUG_LED_LOW DEBUG_LED_PORT &= ~(1<<DEBUG_LED)  ///< DEBUG Led OFF
#endif //COMMON_H