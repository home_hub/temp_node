#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>

#include "uart.h"

#define U2X_SINGLE      1
#define U2X_DOUBLE      0

#define USART0_BAUDRATE         9600
#define USART0_SPEED            U2X_DOUBLE

#define BAUD_PRESCALE ((((F_CPU + (USART0_BAUDRATE * 8UL)) / (USART0_BAUDRATE * 16UL))) - 1)
#define BAUD_2X_PRESCALE ((((F_CPU + (USART0_BAUDRATE * 4UL)) / (USART0_BAUDRATE * 8UL))) - 1) 

void USART_Init(uint16_t baud)
{

LINCR = (1 << LSWRES);                  // Ensure LENA is cleared so LDISR/LBT may be written.

  #if (USART0_SPEED == U2X_SINGLE)
    LINBRRH = (uint8_t) (BAUD_PRESCALE >> 8);
    LINBRRL = (uint8_t) BAUD_PRESCALE;
  #elif (USART0_SPEED == U2X_DOUBLE)
    LINBRRH = (uint8_t) (BAUD_2X_PRESCALE >> 8);
    LINBRRL = (uint8_t) BAUD_2X_PRESCALE;
  #endif

  #if (USART0_SPEED == U2X_SINGLE)
    LINBTR = (1 << LDISR) | (16 << LBT0);   // Set sample rate to 16 (same as AT90CAN U2X_SINGLE).
  #elif (USART0_SPEED == U2X_DOUBLE)
    LINBTR = (1 << LDISR) | (8 << LBT0);    // Set sample rate to 8 (same as AT90CAN U2X_DOUBLE).
  #endif

        // Enable the LIN/UART, set UART Tx byte enable, mode 00 - 8 bit no parity.
    LINCR = (1 << LENA) | (1 << LCMD2) | (1 << LCMD0);   
}

// definiujemy funkcj� dodaj�c� jeden bajtdoz bufora cyklicznego
void uart_putc(char data)
{
	/* Wait for empty transmit buffer */
	while ((LINSIR & (1 << LBUSY)))
		;
	LINDAT = data;
	while ((LINSIR & (1 << LTXOK)))
		;
}

void uart_puts(char *s) // wysy�a �a�cuch z pami�ci RAM na UART
{
	register char c;
	while ((c = *s++))
		uart_putc(c); // dop�ki nie napotkasz 0 wysy�aj znak
}

void uart_putint(int value, int radix) // wysy�a na port szeregowy tekst
{
	char string[25];			// bufor na wynik funkcji itoa
	sprintf(string,"%#x \n",value);
  //itoa(value, string, radix); // konwersja value na ASCII
	uart_puts(string);			// wy�lij string na port szeregowy
}
