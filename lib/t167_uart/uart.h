

#ifndef UART_H_
#define UART_H_

void USART_Init( uint16_t baud );

void uart_putc( char data );
void uart_puts(char *s);
void uart_putint(int value, int radix);

#endif /* UART_H_ */
