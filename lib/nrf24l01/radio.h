#pragma once
/**
 * @file radio.h
 * @author Grzegorz Rezmer <grzes_rezmer AT o2.pl>
 * @version 1.0.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * This library inheritates but customize library from author Nefarious19 so bellow is license from prevorius
 * lib. 
 *  
 *      Created on: 2015-03-07
 *       Modyfied: 2015-04-13 21:18:47
 *       Author: Nefarious19
 *
 *      Project name: "NRF24"
 *
 *      This is AVR GCC library for nRF24L01 module, ver. 1.0
 *      It can be only used by REGISTERED USERS of www.forum.atnel.pl,
 *      they must only leave this header in they C code.
 *     
 *      Library code was based on the libraries from the books:
 *     
 *      https://www.sklep.atnel.pl/pl/p/AVR-Microcontrollers-C-Programming-Basics-EN-BOOK-DVD/103
 *      https://www.sklep.atnel.pl/pl/p/Jezyk-C-Pasja-programowania-mikrokontrolerow-8-bitowych-Wydanie-II-Ksiazka-DVD/104
 *     
 *      "
 *     
 *      This library uses also some ideas from:
 *     
 *      http://gizmosnack.blogspot.com/2013/04/tutorial-nrf24l01-and-avr.html
 *      http://www.tinkerer.eu/AVRLib/nRF24L01/
 *
 *
 * @section DESCRIPTION
 *
 *  <TO-DO>
 *  
 */

#include "../../src/common.h"

enum registers_status_t
{
    E_STATUS,
    E_FIFO,
    E_FEATURE,
    E_CONFIG,
    E_TRANSMIT_SETUP
};

/****** MAIN FUNCTIONS *******/

void radio_RX_EVENT(void);
void radio_register_RX_Callback(void (*callback)(uint8_t *nRF_RX_buff, uint8_t len));

void radio_init();
void radio_reset(void);

void radio_transmit(const void *buffer,uint8_t length);
void radio_transmit_P(const char *buffer,uint8_t length);

void radio_Power_Down(void);

void radio_Clear_TX(void);
void radio_Clear_RX(void);

void radio_RX_Power_Up(void);
void radio_TX_Power_Up(void);

/****** GETER FUNCTIONS *******/

uint8_t getStatus(enum registers_status_t);

/****** SETER FUNCTIONS *******/

void radio_Set_Data_Speed_And_Reciver_Power(uint8_t Data_rate, uint8_t power);
void radio_Set_Active_DataPipe_And_ACK(uint8_t DataPipe, uint8_t on_or_off, uint8_t ACK_on_or_off);
void radio_Set_Dynamic_Payload_State_On_Data_Pipe(uint8_t data_pipe_number, uint8_t on_off);
void radio_Set_Retransmission_Time_And_Ammount(uint8_t time, uint8_t ammount);
void radio_Set_State_And_Width_Of_CRC(uint8_t one_or_two_bytes, uint8_t on_or_off);
void radio_Set_Channel(uint8_t channel);
void radio_set_RXaddr(uint8_t data_pipe, const char *addres);
void radio_set_TXaddr(const char *addres);
