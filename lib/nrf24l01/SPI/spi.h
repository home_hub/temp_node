#pragma once

#include "../../src/common.h"

/******* USI SETUP *******
 * 
 *      spi_mode: 0 - external clock - positive edge, 
 *                1 - external clock - negative edge.  
*/

/**** USE IRQ 
 * 1 - USI over IRQ fandler ON 
 * 0 - USI uver IRQ handler OFF
*/
#define USE_IRQ 0

/******* USI port and pin definitions ********/

#define USI_OUT_REG PORTB       //!< USI port output register.
#define USI_IN_REG PINB         //!< USI port input register.
#define USI_DIR_REG DDRB        //!< USI port direction register.
#define USI_CLOCK_PIN DD_SCK    //!< USI clock I/O pin.
#define USI_DATAIN_PIN DD_DI  //!< USI data input pin.
#define USI_DATAOUT_PIN DD_DO //!< USI data output pin.

/********* IRQ HANDLER SETUP *********/

#if USE_IRQ == 1 
/*  Speed configuration:
 *  Bits per second = CPUSPEED / PRESCALER / (COMPAREVALUE+1) / 2.
 *  Maximum = CPUSPEED / 64.
 */
#define TC0_PRESCALER_VALUE 8 //!< Must be 1, 8, 64, 256 or 1024.
#define TC0_COMPARE_VALUE 125    //!< Must be 0 to 255. Minimum 31 with prescaler CLK/1.

/*  Prescaler value converted to bit settings.
 */
#if TC0_PRESCALER_VALUE == 1
#define TC0_PS_SETTING (1 << CS00)
#elif TC0_PRESCALER_VALUE == 8
#define TC0_PS_SETTING (1 << CS01)
#elif TC0_PRESCALER_VALUE == 64
#define TC0_PS_SETTING (1 << CS01) | (1 << CS00)
#elif TC0_PRESCALER_VALUE == 256
#define TC0_PS_SETTING (1 << CS02)
#elif TC0_PRESCALER_VALUE == 1024
#define TC0_PS_SETTING (1 << CS02) | (1 << CS00)
#else
#error Invalid T/C0 prescaler setting.
#endif
#endif

void init_SPI(uint8_t spi_mode);                                                                             //initialize SPI
uint8_t SPI_WriteReadByte(uint8_t data);                                                         //write/read byte to/from nRF
uint8_t  SPI_WriteByte(uint8_t data);                                                                //write byte to nRF
void SPI_WriteDataBuffer(uint8_t *data_buffer_out, uint8_t length);                              //write to nRF buffer with data
void SPI_WriteReadDataBuffer(uint8_t *data_buffer_in, uint8_t *data_buffer_out, uint8_t length); //write/read to/from nRF buffer with data
