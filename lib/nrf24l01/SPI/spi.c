#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "spi.h"
#include "nRF24L01_memory_map.h"
#include "../../src/common.h"

#if USE_IRQ == 1
/*! \brief  Data input register buffer.
 *
 *  Incoming bytes are stored in this byte until the next transfer is complete.
 *  This byte can be used the same way as the SPI data register in the native
 *  SPI module, which means that the byte must be read before the next transfer
 *  completes and overwrites the current value.
 */
volatile unsigned char storedUSIDR;

/*! \brief  Driver status bit structure.
 *
 *  This struct contains status flags for the driver.
 *  The flags have the same meaning as the corresponding status flags
 *  for the native SPI module. The flags should not be changed by the user.
 *  The driver takes care of updating the flags when required.
 */

struct usidriverStatus_t
{
    unsigned char masterMode : 1;       //!< True if in master mode.
    unsigned char transferComplete : 1; //!< True when transfer completed.
    unsigned char writeCollision : 1;   //!< True if put attempted during transfer.
};

volatile struct usidriverStatus_t spiX_status; //!< The driver status bits.

#if defined(__AVR_ATtiny2313__)

/*! \brief  Timer/Counter 0 Compare Match Interrupt handler.
 *
 *  This interrupt handler is only enabled when transferring data
 *  in master mode. It toggles the USI clock pin, i.e. two interrupts
 *  results in one clock period on the clock pin and for the USI counter.
 */

ISR(TIMER0_COMPA_vect)
{
    USICR |= (1 << USITC); // Toggle clock output pin.
}

/*! \brief  USI Timer Overflow Interrupt handler.
 *
 *  This handler disables the compare match interrupt if in master mode.
 *  When the USI counter overflows, a byte has been transferred, and we
 *  have to stop the timer tick.
 *  For all modes the USIDR contents are stored and flags are updated.
 */

ISR(USI_OVERFLOW_vect)
{
    // Master must now disable the compare match interrupt
    // to prevent more USI counter clocks.
    if (spiX_status.masterMode == 1)
    {
        TIMSK &= ~(1 << OCIE0A);
    }
    // Update flags and clear USI counter
    USISR = _BV(USIOIF);
    spiX_status.transferComplete = 1;
    // Copy USIDR to buffer to prevent overwrite on next transfer.
    storedUSIDR = USIDR;
}

static inline void spi_wait(void)
{
    do
    {

    } while (spiX_status.transferComplete == 0);
}
#endif //AVR_TYPE
#endif //USE_IRQ

/******* USI SETUP *******
 * 
 *      spi_mode: 0 - external clock - positive edge, 
 *                1 - external clock - negative edge.  
*/
void init_SPI(uint8_t spi_mode)
{
#if defined(__AVR_ATtiny167__)
    DDR_SPI |= ((1 << DD_MOSI) | (1 << DD_SCK) | (1 << DD_SS)); //SET pins MOSI, CSN(chip select),SCK, i CE(control RX, TX nRFA) as ouputs
    SPCR |= (0 << SPIE) |                                       //turn OFF SPI intterrupt
            (1 << SPE) |                                        //enable SPI module
            (0 << DORD) |                                       //set data order
            (1 << MSTR) |                                       //enable SPI in master mode
            (0 << CPOL) |                                       //SCK polarity, rising edge is active
            (0 << CPHA) |                                       //data polarity, read data on rising edge
            (1 << SPR0) |                                       //SCK speed
            (0 << SPR1);                                        //
    SPSR |= (1 << SPI2X);                                       //SPI sck 2 times faster
#elif defined(__AVR_ATtiny2313__)

    USI_DIR_REG |= (1 << USI_DATAOUT_PIN) | (1 << USI_CLOCK_PIN); // Outputs.
    USI_DIR_REG &= ~(1 << USI_DATAIN_PIN);                        // Inputs.
    USI_OUT_REG |= (1 << USI_DATAIN_PIN);                         // Pull-ups.

#if USE_IRQ == 1
                                                                  // Configure port directions.

    // Configure USI to 3-wire master mode with overflow interrupt.

    USICR = (1 << USIOIE) | (1 << USIWM0) |
            (1 << USICS1) | (spi_mode << USICS0) |
            (1 << USICLK);

    // Enable 'Clear Timer on Compare match' and init prescaler.
    // This made it impossible to get interrupts for some reason
    TCCR0A = (1 << WGM01);
    TCCR0B = TC0_PS_SETTING;
    // Init Output Compare Register.
    OCR0A = TC0_COMPARE_VALUE;
    // Init driver status register.

    spiX_status.masterMode = 1;
    spiX_status.transferComplete = 0;
    spiX_status.writeCollision = 0;
    storedUSIDR = 0;
#elif USE_IRQ == 0
    USICR |= ((1 << USIWM0) | (1 << USICS1) | (spi_mode << USICS0) | (1 << USICLK)); // Configure 3-wire mode with external clock and USICT ac clock source.
#endif
#endif
}

uint8_t SPI_WriteReadByte(uint8_t data)
{
#if defined(__AVR_Attiny167__)
    SPDR = data; //send byte to SPDR register
    while (!(SPSR & (1 << SPIF)))
        ; //wait for data shifting
#elif defined(__AVR_ATtiny2313__)
#if USE_IRQ == 1
    SPI_WriteByte(data);
    return storedUSIDR;
#elif USE_IRQ == 0
    SPI_WriteByte(data);
    return USIDR;
#endif
#endif
}

uint8_t SPI_WriteByte(uint8_t data)
{
#if defined(__AVR_Attiny167__)
    SPDR = data; //send byte to SPDR register
    while (!(SPSR & (1 << SPIF)))
        ; //wait for data shifting
#elif defined(__AVR_ATtiny2313__)
#if USE_IRQ == 1
    // Check if transmission in progress,
    // i.e. USI counter unequal to zero.
    if ((USISR & 0x0F) != 0)
    {
        // Indicate write collision and return.
        spiX_status.writeCollision = 1;
        return 0;
    }
    // Reinit flags.
    spiX_status.transferComplete = 0;
    spiX_status.writeCollision = 0;
    // Put data in USI data register.
    USIDR = data;
    // Master should now enable compare match interrupts.
    if (spiX_status.masterMode == 1)
    {
        TIFR |= (1 << OCF0A);   // Clear compare match flag.
        TIMSK |= (1 << OCIE0A); // Enable compare match interrupt.
    }
    if (spiX_status.writeCollision == 0)
    {
        spi_wait();
        return 1;
    }
    return 0;
#elif USE_IRQ == 0
    USIDR = data; //Write byte to register

    USISR |= _BV(USIOIF); // clear flag to receive new data;

    while ((USISR & (1 << USIOIF)) == 0) //Toggling SCK ppin using 4-bit counter to send tada to slave.
    {
        USICR |= _BV(USITC);
    }
    return 1;
#endif
#endif
}

void SPI_WriteDataBuffer(uint8_t *data_buffer_out, uint8_t length)
{
#if defined(__AVR_Attiny167__)
    uint8_t *pointer;            //declare pointer
    uint8_t i;                   //declare indexing variable
    pointer = data_buffer_out;   //pointer is showing on first byte of "data_buffer_out"
    for (i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        SPI_WriteByte(*pointer++); //send bytes to nRF
    }

#elif defined(__AVR_ATtiny2313__)
    uint8_t *pointer;          //declare pointer
    uint8_t i;                 //declare indexing variable
    pointer = data_buffer_out; //pointer is showing on first byte of "data_buffer_out"

    for (i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        SPI_WriteByte(*pointer++); //send bytes to nRF
    }

#endif
}

void SPI_WriteReadDataBuffer(uint8_t *data_buffer_in, uint8_t *data_buffer_out, uint8_t length)
{
#if defined(__AVR_Attiny167__)

    for (uint8_t i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        *data_buffer_out++ = SPI_WriteReadByte(*data_buffer_out++); //send bytes to nRF and read data back from nRF
    }

#elif defined(__AVR_ATtiny2313__)
#if USE_IRQ == 1

    for (uint8_t i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        SPI_WriteByte(*data_buffer_out++); //send bytes to nRF and read data back from nRF
        *pointer2++ = storedUSIDR;
    }
#elif USE_IRQ == 0

    for (uint8_t i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        SPI_WriteByte(*data_buffer_in++); //send bytes to nRF and read data back from nRF
        *data_buffer_out = USIDR;
    }
    
#endif
#endif
}
