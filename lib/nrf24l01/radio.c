/**
 * 
 * @author Grzegorz Rezmer <grzes_rezmer AT o2.pl>
 * @version 1.0.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * This library inheritates but customize library from author Nefarious19 so bellow is license from prevorius
 * lib. 
 *  
 *      Created on: 2015-03-07
 *       Modyfied: 2015-04-13 21:18:47
 *       Author: Nefarious19
 *
 *      Project name: "NRF24"
 *
 *      This is AVR GCC library for nRF24L01 module, ver. 1.0
 *      It can be only used by REGISTERED USERS of www.forum.atnel.pl,
 *      they must only leave this header in they C code.
 *     
 *      Library code was based on the libraries from the books:
 *     
 *      https://www.sklep.atnel.pl/pl/p/AVR-Microcontrollers-C-Programming-Basics-EN-BOOK-DVD/103
 *      https://www.sklep.atnel.pl/pl/p/Jezyk-C-Pasja-programowania-mikrokontrolerow-8-bitowych-Wydanie-II-Ksiazka-DVD/104
 *     
 *      "
 *     
 *      This library uses also some ideas from:
 *     
 *      http://gizmosnack.blogspot.com/2013/04/tutorial-nrf24l01-and-avr.html
 *      http://www.tinkerer.eu/AVRLib/nRF24L01/
 *
 *
 * @section DESCRIPTION
 *
 *  <TO-DO>
 *  
 */

#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>
#include <avr/pgmspace.h>

#include "radio.h"
#include "nRF24L01_memory_map.h"
#include "SPI/spi.h"

#define MAX_RT_MASK (0 << MASK_MAX_RT)
#define TX_DS_MASK (0 << MASK_TX_DS)
#define RX_DR_MASK (0 << MASK_RX_DR)

volatile static uint8_t rx_flag;
volatile static uint8_t tx_flag;

uint8_t rx_buffer[BUFFER_SIZE + 1];
//..................................................................................................................
volatile static uint8_t payload_width;
volatile static uint8_t first_time = 0;
//_______________________________________________________________

static void radio_set_reg(uint8_t reg, uint8_t value);
static void radio_write_data_reg(uint8_t reg, uint8_t *data, uint8_t len);
static uint8_t radio_get_reg(uint8_t reg);
static void radio_setup(void);
static void interupt_init();

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// callback function declaration
//........................................................................................................................
static void (*nRF_RX_Event_Callback)(uint8_t *nRF_RX_buff, uint8_t len);
//________________________________________________________________________________________________________________________

void radio_init()
{
    DDR_CE_CSN |= (_BV(CSN_PIN) | _BV(CE_PIN));

    init_SPI(0);

    interupt_init();

    sei();

    radio_setup();

    CE_HI;
    CSN_HI;
}

void radio_RX_EVENT(void)
{   
    if (rx_flag) // if RX_flag == 1 then: - jeżeli zmienna RX_flag == 1 to:
    {
        uint8_t fifo_status; //variable which stores FIFO_STATUS register value
        uint8_t len;         //variable which stores length of recived payload
        uint8_t i;           //index variable for loop
        uint8_t *wsk;        //pointer for first byte of reciver buffer nRF_RX_buffer
        rx_flag = 0;         //reset the RX_flag

        //do...while loop. this loop executes until RX_FIFO is empty
        do
        {
            radio_set_reg(STATUS, (1 << RX_DR)); //Reset RX_DR IRQ flag in nRF STATUS register
            wsk = rx_buffer;                     //wsk equals to addres of first byte nRF_RX_bufffer
            CSN_LOW;                             //chip select low
            SPI_WriteByte(R_RX_PL_WID);          //send: read length of payload command
            len = SPI_WriteReadByte(NOP);        //read length
            CSN_HI;                              //chip select high

            if (len > MAXIMUM_PAYLOAD_SIZE)
                break; //if len is bigger than maximum payload size then break

            CSN_LOW;                     //chip select low
            SPI_WriteByte(R_RX_PAYLOAD); //read payload command
            for (i = 0; i < len; i++)    //read data until i < len
            {
                *wsk++ = SPI_WriteReadByte(NOP);
            }

            CSN_HI;                                     //csn goes high - csn stan wysoki
            fifo_status = radio_get_reg(FIFO_STATUS);   //read FIFO_STATUS register
        } while ((fifo_status & (1 << RX_EMPTY)) == 0); //if RX_EMPTY bit is LOW then loop must execute.
        radio_Clear_RX();                               //clear RX_FIFO
        radio_set_reg(STATUS, (1 << RX_DR));            //Reset RX_DR IRQ flag in nRF STATUS register
        if (len && nRF_RX_Event_Callback)
            (*nRF_RX_Event_Callback)(rx_buffer, len); //if callback function is registered and len is bigger
                                                      //than 0 send recived data to your callback function
    }
}

uint8_t getStatus(enum registers_status_t stat)
{
    switch (stat)
    {
    case E_CONFIG:
        return radio_get_reg((CONFIG));
    case E_STATUS:
        return radio_get_reg((STATUS));
    case E_FEATURE:
        return radio_get_reg((EN__AA));
    case E_FIFO:
        return radio_get_reg((FIFO_STATUS));
    case E_TRANSMIT_SETUP:
        return radio_get_reg((RF_SETUP));
    default:
        return 0x00;
    }
    return 0x00;
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function which is used to register your own callback function
//........................................................................................................................
void radio_register_RX_Callback(void (*callback)(uint8_t *nRF_RX_buff, uint8_t len))
{
    nRF_RX_Event_Callback = callback; //in this line we give an addres to our callback function
}

void radio_set_RXaddr(uint8_t data_pipe, const char *addres)
{
    radio_write_data_reg(data_pipe, (uint8_t *)addres, ADDRES_LENGTH);
}

void radio_set_TXaddr(const char *adr)
{
    radio_write_data_reg(TX_ADDR, (uint8_t *)adr, ADDRES_LENGTH); //else send given by attribute ammount of bytes
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to send data to another radio's
// attribute to this function is the pointer to the first byte of transsmision buffer
//........................................................................................................................
void radio_transmit(const void *buffer,uint8_t length)
{
    if (tx_flag)
        return; //if still is in transmitting mode or function isn't used for the first time, return
    
    tx_flag = 1; //set TX mode flag to one

    radio_Clear_TX();    //clear TX fifo
    radio_TX_Power_Up(); //turn on transmission mode

    CSN_LOW;                             //make CSN low
    SPI_WriteByte(W_TX_PAYLOAD);         //send to radio W_TX_PAYLOAD command what means "Write data to TX FIFO"
    for (uint8_t i = 0; i < length; i++) //until "i" is smaller than "length" send bytes to radio
    {
        SPI_WriteByte(*(uint8_t*)buffer++);
    }

    CSN_HI; //CSN high
     
    CE_HI; //start transmision!
    _delay_us(25);
    CE_LOW;
}

void radio_transmit_P(const char *buffer,uint8_t length)
{
    if (tx_flag)
        return; //if still is in transmitting mode or function isn't used for the first time, return
    
    tx_flag = 1; //set TX mode flag to one

    radio_Clear_TX();    //clear TX fifo
    radio_TX_Power_Up(); //turn on transmission mode

    CSN_LOW;                             //make CSN low
    SPI_WriteByte(W_TX_PAYLOAD);         //send to radio W_TX_PAYLOAD command what means "Write data to TX FIFO"
    for (uint8_t i = 0; i < length; i++) //until "i" is smaller than "length" send bytes to radio
    {
        SPI_WriteByte(pgm_read_byte(buffer++));
    }

    CSN_HI; //CSN high
     
    CE_HI; //start transmision!
    _delay_us(25);
    CE_LOW;
}

void radio_reset(void)
{
    CSN_LOW;
    SPI_WriteByte((W_REGISTER | STATUS));
    SPI_WriteByte(0x70);
    CSN_HI;
}

static void radio_setup(void)
{
    radio_write_data_reg(TX_ADDR, (uint8_t *)ADDRES,ADDRES_LENGTH);    //set transmitter addres
    radio_write_data_reg(RX_ADDR_P0, (uint8_t *)ADDRES,ADDRES_LENGTH); //set reciving addres for datapipe0
    radio_set_reg(CONFIG, (RX_DR_MASK | MAX_RT_MASK | TX_DS_MASK));           //Write interrupt masks radio_CONFIG to CONGIG register of radio
    radio_Clear_RX();                                                         //Clear RX FIFO
    radio_Clear_TX();                                                         //Clear TX FIFO
    radio_Set_State_And_Width_Of_CRC(ONE_BYTE, ON);                           //ON == Enable CRC; OFF == disable CRC, ONE_BYTE or TWO_BYTES - width of CRC;
    radio_set_reg(RF_CH, TRANSMITT_CHANNEL);                                  //Set channel number
    radio_Set_Active_DataPipe_And_ACK(ERX_P0, ON, ACK_ON);                    //Set which datapipe state you want to change, in this case datapipe = 0 (ERX_P0), ON - means enable this datapipe, ACK_ON means to enable ACK for choosen data pipe.
    radio_set_reg(SETUP_RETR, (WAIT_4000uS || RETR_15_TIMES));                //set time between retransmissions and ammount of retranssmisions
    radio_set_reg(RF_SETUP, (TRANS_SPEED_1MB | RF_PWR_0dB));                  //Set transmision speed and reciver power
    radio_set_reg(DYNPD, DPL_P0);                                             //set dynamic payload of data pipe
    radio_set_reg(FEATURE, _BV(EN_DPL));                                      //set feture to use dyn payload.

    rx_flag = 0; //Clear RX flag
    tx_flag = 0; //Clear TX flag

    radio_set_reg(STATUS, (1 << TX_DS) | (1 << RX_DR) | (1 << MAX_RT)); //Clear interrupt bits in STATUS register
    radio_set_reg(CONFIG, (1 << PWR_UP) | (1 << PRIM_RX));              //Start up ass RX
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to set transmission channel
//........................................................................................................................
void radio_Set_Channel(uint8_t channel)
{
    radio_set_reg(RF_CH, channel); //save to radio RF_CH register channel number (0x7f == 0b01111111 is the mask)
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to number of retransmissions and time beetwen each retransmission
// first attribute is WAIT_XXXX_uS and the second is RETR_X_TIMES (see radio_memory_map.h)
//........................................................................................................................
void radio_Set_Retransmission_Time_And_Ammount(uint8_t time, uint8_t ammount)
{
    radio_set_reg(SETUP_RETR, (time | ammount)); //write to SETUP_RETR register number of retransmisions and time beetwen them
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to set states of datapipes
// first attribute is DataPipe, you must put here name of datapipe tha you want to enable, this name is ERX_Px where x is number of data pipe (see radio_Init function)
// second attribute is or_on_off, here you put ON or OFF macro. When you put here ON, the choosen data pipe will be turned ON, if put here OFF, datapipe will be turned off
// third and last parameter is ACK_on_or_off, you must put here macros: ACK_ON or ACK_OFF. This parameter turns ACK_ON or turns ACK_OFF
//........................................................................................................................
void radio_Set_Active_DataPipe_And_ACK(uint8_t DataPipe, uint8_t on_or_off, uint8_t ACK_on_or_off)
{
    uint8_t data = radio_get_reg(EN_RXADDR); //read data from EN_RXADDR register save it in data variable

    if (on_or_off == ON)
        data |= (DataPipe); //if second attribute is ON, set bit ERX_Px (given by DataPipe parameter)
    else if (on_or_off == OFF)
        data &= ~(DataPipe); //else reset this bit

    radio_set_reg(EN_RXADDR, data); //save data variable to EN_RXADDR register

    data = radio_get_reg(EN__AA); //read data from EN_AA register and save it in data variable

    if (ACK_on_or_off == ACK_ON)
        data |= (DataPipe); //if third parameter is ACK_ON, enable auto ackonwledgements by setting ENAA_Px bit (this same value as ERX_Px)
    else if (ACK_on_or_off == ACK_OFF)
        data &= ~(DataPipe); //else turn off auto acknowledgments

    radio_set_reg(EN__AA, data); //save the variable data to EN__AA register
}
//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to set dynamic payloads
// first attribute is DPL_Px (number of receiver payload) and the second is ON or OFF(see radio_memory_map.h)
//........................................................................................................................

void radio_Set_Dynamic_Payload_State_On_Data_Pipe(uint8_t data_pipe_number, uint8_t on_off)
{
    radio_set_reg(DYNPD, data_pipe_number);   //save DPL_Px value to DYNPD register
    uint8_t feature = radio_get_reg(FEATURE); //make copy of FEATURE register

    if (on_off == ON)
        feature |= (1 << EN_DPL); //if second attribute is equal to ON
                                  //set bit EN_DPL
    if (on_off == OFF)
        feature &= ~(1 << EN_DPL);   //if second attribute is equal to OFF
                                     //reset bit EN_DPL
    radio_set_reg(FEATURE, feature); //save feature variable to FEATURE register
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to set state and width of CRC. First attrbiute of this function is ONE_BYTE or TWO_BYTES,
// second attribute is ON or OFF
//........................................................................................................................
void radio_Set_State_And_Width_Of_CRC(uint8_t one_or_two_bytes, uint8_t on_or_off)
{
    uint8_t config = radio_get_reg(CONFIG); //read data from congig register and save it to config variable

    if (one_or_two_bytes == ONE_BYTE)
        config &= ~(1 << CRCO); //if first given attribute is ONE_BYTE clear CRCO bit
    else if (one_or_two_bytes == TWO_BYTES)
        config |= (1 << CRCO); //else if first given attribute is TWO_BYTES set CRCO bit
    else
        config &= ~(1 << CRCO); //else given attribute is diffrent set CRCO low

    if (on_or_off == ON)
        config |= (1 << EN_CRC); //if second attribute is equal to ON, set EN_CRC bit
    else if (on_or_off == OFF)
        config &= ~(1 << EN_CRC); //esle reset the EN_CRC bit

    radio_set_reg(CONFIG, config); //write config content to CONFIG register
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to switch radio into TX mode
//........................................................................................................................
void radio_TX_Power_Up(void)
{
    CE_LOW;                         //make CE low
    uint8_t config;                 //declaration of temporary variable
    config = radio_get_reg(CONFIG); //save copy of config regster in config variable

    config &= ~(1 << PRIM_RX); //reset PRIM_RX bit
    config |= (1 << PWR_UP);   //set PWR_UP bit

    radio_set_reg(CONFIG, config); //write config variable to CONGIG register
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to switch radio into RX mode
//........................................................................................................................
void radio_RX_Power_Up(void)
{
    CE_HI;  
    radio_set_reg(CONFIG, (1 << PWR_UP) | (1 << PRIM_RX)); //set PWR_UP and PRIM_RX bits in config variable ande save it in radio CONFIG register
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to switch radio into POWER DOWN mode
//........................................................................................................................
void radio_Power_Down(void)
{
    CE_LOW;                                      //make CE_LOW
    uint8_t config;                              //declaration of config variable
    config = radio_get_reg(CONFIG);              //save copy of CONFIG register to config variable
    config &= ~((1 << PWR_UP) | (1 << PRIM_RX)); //reset PWR_UP and PRIM_RX bits
    radio_set_reg(CONFIG, config);               //save congig variable in CONFIG register
    radio_Clear_RX();                            //clear RX fifo
    radio_Clear_TX();                            //clear TX fifo
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to clear TX fifo
//........................................................................................................................
void radio_Clear_TX(void)
{
    CSN_LOW;                 //make CSN low
    SPI_WriteByte(FLUSH_TX); //send to radio FLUSH_TX command what means CLEAR TX FIFO
    CSN_HI;                  //make CSN high
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// function used to clear RX fifo
//........................................................................................................................
void radio_Clear_RX(void)
{
    CSN_LOW;                 //make CSN low
    SPI_WriteByte(FLUSH_RX); //send to radio FLUSH_RX command what means CLEAR RX FIFO
    CSN_HI;                  //make CSN high
}

void radio_Set_Data_Speed_And_Reciver_Power(uint8_t Data_rate, uint8_t power)
{
    radio_set_reg(RF_SETUP, (Data_rate | power)); //write to RF_SETUP register transmision speed and and receiver power
}

static void radio_set_reg(uint8_t reg, uint8_t value)
{
    CSN_LOW;
    SPI_WriteByte(W_REGISTER | reg);
    _delay_us(10);
    SPI_WriteByte(value);
    CSN_HI;
}

static void radio_write_data_reg(uint8_t reg, uint8_t *data, uint8_t len)
{
    CSN_LOW;
    SPI_WriteByte(W_REGISTER | reg);

    for (uint8_t i = 0; i < len; i++)
    {
        SPI_WriteByte(data[i]);
    }
    CSN_HI;
}

uint8_t radio_get_reg(uint8_t reg)
{
    uint8_t temp = 0;
    CSN_LOW;
    SPI_WriteByte(R_REGISTER | reg);
    _delay_us(10);
    temp = SPI_WriteReadByte(NOP);
    CSN_HI;
    return temp;
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
// Interupt setup. Depending on AVR CPU you are using, need to setup INT or PCINT interupt.
// Remember to setup proper interupt vector in ISR function.
//........................................................................................................................
void interupt_init()
{
#if defined(__AVR_ATmega328P__) || (__AVR_ATmega328__)
    GICR |= _BV(INT2);   //enable INT2 intterupt source in GICR register
    MCUCSR |= _BV(ISC2); //set intterupt active on falling edge
#elif defined(__AVR_ATtiny2313__)
    GIMSK |= _BV(INT0);  // interupt on INT0
    MCUCR |= _BV(ISC01); // falling edge interupt
    MCUCR &= ~_BV(ISC00);
#endif
}

ISR(INT0_vect)
{
    CSN_LOW;                                         //Chip select LOW
    volatile uint8_t status = radio_get_reg(STATUS); //read STATUS register value
    CSN_HI;                                          //chip select high

    if ((status & _BV(RX_DR))) //id data recived
    {
        radio_set_reg(STATUS, _BV(RX_DR)); //save status variable in STATUS register
        rx_flag = 1;                   //RX flag equals to 1
    }
    else if (status & _BV(TX_DS)) //if data was send
    {
        radio_RX_Power_Up();           //enable receiving mode
        radio_set_reg(STATUS, _BV(TX_DS)); //save status variable in STATUS register
        tx_flag = 0;                   //TX_flag = 0
    }
    else if (status & _BV(MAX_RT)) //id max of retransmissions was achived
    {
        radio_RX_Power_Up();                  //turn on RX mode
        radio_Clear_TX();                     //clear TX fifo
        radio_set_reg(STATUS, (_BV(MAX_RT) | _BV(TX_DS)));        //save variable status to the STATUS register
        tx_flag = 0;                          //clear TX_flag
    }
}