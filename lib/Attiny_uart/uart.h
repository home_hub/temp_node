

#ifndef UART_H_
#define UART_H_

void USART_Init();

void uart_putc(char data );
void uart_puts(char *string);
void uart_putint(int value, int radix);

uint8_t uart_receive( void );

#endif /* UART_H_ */
