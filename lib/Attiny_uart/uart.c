#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>

#include "uart.h"

#define USE_2X 1

#define USART_BAUDRATE 9600UL
#define USART0_SPEED U2X_DOUBLE

#define PARITY_NONE 0
#define DATA_BITS_8 ((1 << UCSZ1) | (1 << UCSZ0))
#define STOP_BITS_1 0

#if USE_2X
#define _UBBR_ ((F_CPU / (8 * USART_BAUDRATE)) - 1)
#else
#define _UBBR_ ((F_CPU / (16 * USART_BAUDRATE)) - 1)
#endif

void USART_Init()
{
	unsigned int ubbr = _UBBR_;
	UBRRH = (uint8_t)(ubbr >> 8);
	UBRRL = (uint8_t)ubbr;

#if USE_2X
	UCSRA |= (1 << U2X);
#else
	UCSRA &= ~(1 << U2X);
#endif

	UCSRB |= (1 << TXEN);
	UCSRC |= (DATA_BITS_8 | PARITY_NONE | STOP_BITS_1);
}

// definiujemy funkcj� dodaj�c� jeden bajtdoz bufora cyklicznego
void uart_putc(char data)
{
	/* Wait for empty transmit buffer */
	while (!(UCSRA & (1 << UDRE)))
		;
	/* Put data into buffer, sends the data */
	UDR = data;
}

void uart_puts(char *string) // wysy�a �a�cuch z pami�ci RAM na UART
{
	while (*string)
	{
		uart_putc(*string);
		string++;
	} // dop�ki nie napotkasz 0 wysy�aj znak
}

uint8_t uart_receive(void)
{
	while (!(UCSRA & (1 << RXC)))
		;		/* Wait for data to be received */
	return UDR; /* Get and return received data from buffer */
}

void uart_putint(int value, int radix) // wysy�a na port szeregowy tekst
{
	char string[25];			// bufor na wynik funkcji itoa
	itoa(value, string, radix); // konwersja value na ASCII
	uart_puts(string);			// wy�lij string na port szeregowy
}
